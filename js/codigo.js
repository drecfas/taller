// Función para acceso a la Base de Datos
function cerrar(obj){
	obj = "." + obj;
	$(obj).remove();
}

// Login
function ingresar(){
	var datos={	"usuario":$("#usuario").val(), 
				"clave":$("#clave").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'login-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

// Usuarios
function listarUsuarios(){
	$.ajax({
		type:'post',
		url:'usuario-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoUsuario(){
	var datos={	"id_usuario":$("#id_usuario").val(), 
				"nombre":$("#nombre").val(), 
				"clave":$("#clave").val(),
				"confirmacion":$("#confirmacion").val(),
				"fecha_registro":$("#fecha_registro").val(),
				"estado":$("#estado").val(),
				"perfil":$("#perfil").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'usuario-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarUsuario(){
	var datos={	"id_usuario":$("#id_usuario").val(), 
				"nombre":$("#nombre").val(), 
				"clave":$("#clave").val(),
				"confirmacion":$("#confirmacion").val(),
				"fecha_registro":$("#fecha_registro").val(),
				"estado":$("#estado").val(),
				"perfil":$("#perfil").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'usuario-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarUsuario(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'usuario-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Unidad
function listarUnidad(){
	$.ajax({
		type:'post',
		url:'unidad-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevaUnidad(){
	var datos={	"nombre":$("#nombre").val(), 
				"sigla":$("#sigla").val(),
				
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'unidad-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarUnidad(){
	var datos={	"id_unidad":$("#id_unidad").val(), 
				"nombre":$("#nombre").val(), 
				"sigla":$("#sigla").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'unidad-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarUnidad(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'unidad-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Taller
function listarTaller(){
	$.ajax({
		type:'post',
		url:'taller-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoTaller(){
	var datos={	"nombre":$("#nombre").val(), 
				"direccion":$("#direccion").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"ruc":$("#ruc").val(),
				"email":$("#email").val(),
				"estado":$("#estado").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'taller-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarTaller(){
	var datos={	"id_taller":$("#id_taller").val(), 
				"nombre":$("#nombre").val(), 
				"direccion":$("#direccion").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"ruc":$("#ruc").val(),
				"email":$("#email").val(),
				"estado":$("#estado").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'taller-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}
function borrarTaller(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'taller-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}
// Producto/Servicio
function listarProductoServicio(){
	$.ajax({
		type:'post',
		url:'prodserv-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevoProductoServicio(){
	var datos={	"codigo":$("#codigo").val(),
				"nombre":$("#nombre").val(),
				"descripcion":$("#descripcion").val(),
				"id_clasificacion":$("#id_clasificacion").val(),
				"id_unidad":$("#id_unidad").val(),
				"id_marca":$("#id_marca").val(),
				"precio_venta":$("#precio_venta").val(),
				"habilitado":$("#habilitado").val(),
				"iva":$("#iva").val()
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'prodserv-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarProductoServicio(){
	var datos={	"id_servicio_producto":$("#id_servicio_producto").val(), 
				"codigo":$("#codigo").val(), 
				"nombre":$("#nombre").val(),
				"descripcion":$("#descripcion").val(),
				"id_clasificacion":$("#id_clasificacion").val(),
				"id_unidad":$("#id_unidad").val(),
				"id_marca":$("#id_marca").val(),
				"precio_venta":$("#precio_venta").val(),
				"habilitado":$("#habilitado").val(),
				"iva":$("#iva").val(),
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'prodserv-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarProductoServicio(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'prodserv-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}

// Clasificacion
function listarClasificacion(){
	$.ajax({
		type:'post',
		url:'clasificacion-lista-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function nuevaClasificacion(){
	var datos={	"nombre":$("#nombre").val(), 
				
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'clasificacion-nuevo-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function editarClasificacion(){
	var datos={	"id_clasificacion":$("#id_clasificacion").val(), 
				"nombre":$("#nombre").val(), 
		};
	$.ajax({
		data:datos,
		type:'post',
		url:'clasificacion-editar-procesar.php',
		success:function(response){
			$("#rs-ajax").html(response);
		}		
	});	
}

function borrarClasificacion(id){
	var opcion = confirm('Desea Eliminar?');
	if(opcion){
		var datos={"id":id};
		$.ajax({
			data:datos,
			type:'post',
			url:'clasificacion-borrar-procesar.php',
			success:function(response){
				$("#rs-borrar").html(response);
			}		
		});
	}
}
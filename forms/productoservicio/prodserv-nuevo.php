<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nuevo Producto/Servicio</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Código</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="codigo" name="codigo"
						placeholder="Codigo" maxlength="20">
						<small id="codigo_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Nombre</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="nombre" name="nombre"
						placeholder="Nombre" maxlength="50">
						<small id="nombre_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Descripción</label>
					<div class="col-xs-1">
						<!-- <input type="text" class="form-control" id="descripcion" name="descripcion"
						placeholder="Descripcion" maxlength="50"> -->
						<textarea class="form-control" id="descripcion" name="descripcion"></textarea>
						<small id="descripcion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Clasificación</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_clasificacion","select id_clasificacion,nombre from clasificaciones order by 2",
						0,""); ?>
						<small id="id_clasificacion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Unidad</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_unidad","select id_unidad, nombre from unidades order by 2",0,""); ?>
						<small id="id_unidad_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Marca</label>
					<div class="col-xs-1">
						<?php fn_lista_combo("id_marca","select id_marca, nombre from marcas order by 2",0,""); ?>
						<small id="id_marca_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Precio Venta</label>
					<div class="col-xs-1">
						<input type="number" class="form-control" id="precio_venta" name="precio_venta"
						placeholder="Entre 0 y 999.999.999" maxlength="12" value="0">
						<small id="precio_venta_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Habilitado</label>
					<div class="col-xs-1">
						<select id="habilitado" name="habilitado" class="form-control">
							<option value="S" selected>Sí</option>
							<option value="N">No</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="">IVA</label>
					<div class="col-xs-1">
						<select id="iva" name="iva" class="form-control">
							<option value="0">Exento</option>
							<option value="1">IVA 5%</option>
							<option value="2" selected>IVA 10%</option>
						</select>
						<small id="iva_ayuda" class="form-text text-muted"></small>
					</div>
				</div>				
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevoProductoServicio();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='prodserv-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#id_clasificacion").select2();
		$("#id_unidad").select2();
		$("#id_marca").select2();
		$("#habilitado").select2();
		$("#iva").select2();
		$("#codigo").focus();
	</script>
</html>
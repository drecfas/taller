<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			date_default_timezone_set("America/Asuncion");
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_sesion_administrador();
			fn_menu();
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Nuevo Usuario</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Usuario</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="id_usuario" name="id_usuario" 
						placeholder="ID de Usuario" maxlength="20">
						<small id="id_usuario_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Nombre</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="nombre" name="nombre"
						placeholder="Nombre" maxlength="50">
						<small id="nombre_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Clave</label>
					<div class="col-xs-1">
						<input type="password" class="form-control" id="clave" name="clave"
						placeholder="Clave">
						<small id="clave_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Confirmación</label>
					<div class="col-xs-1">
						<input type="password" class="form-control" id="confirmacion" name="confirmacion"
						placeholder="Confirmación">
						<small id="confirmacion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Registro</label>
					<div class="col-xs-1">
						<input type="datetime" class="form-control" id="fecha_registro" 
						name="fecha_registro" value="<?php echo date("d/m/Y H:m");?>" 
						placeholder="Confirmación" disabled>
						<small id="fecha_registro_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="estado" class="form-control">
							<option value="I">Inactivo</option>
							<option value="A" selected>Activo</option>
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Perfil</label>
					<div class="col-xs-1">
						<select id="perfil" class="form-control">
							<option value="A">Administrador</option>
							<option value="C">Consulta</option>
							<option value="O" selected>Operativo</option>
						</select>
						<small id="perfil_ayuda" class="form-text text-muted"></small>
					</div>
				</div>				
				<br>
				<button type="button" class="btn btn-primary" onclick="nuevoUsuario();">Registrar</button>
				<button type="button" class="btn btn-success" onclick="location.href='usuario-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#estado").select2();
		$("#perfil").select2();
		$("#id_usuario").focus();
	</script>
</html>
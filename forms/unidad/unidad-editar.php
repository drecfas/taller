<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
			
			include_once("../../clases/Unidad.php");
			$id = $_GET["id"];
			$unidad = new Unidad();
			$unidad->recuperarUnidad($id);
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Editar Unidad</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Unidad</label>
					<div class="col-xs-1">
						<input type="int" class="form-control" id="id_unidad" name="id_unidad" 
						placeholder="ID de Unidad" maxlength="20" value="<?php echo $unidad->getIdUnidad(); ?>"
						readonly>
						<small id="id_unidad_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Nombre</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="nombre" name="nombre"
						placeholder="Nombre" maxlength="50" value="<?php echo $unidad->getNombre(); ?>">
						<small id="nombre_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Sigla</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="sigla" name="sigla"
						placeholder="Sigla" maxlength="50" value="<?php echo $unidad->getSigla(); ?>">
						<small id="sigla_ayuda" class="form-text text-muted"></small>
					</div>
				</div>		
				<br>
				<button type="button" class="btn btn-primary" onclick="editarUnidad();">Editar</button>
				<button type="button" class="btn btn-success" onclick="location.href='unidad-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#estado").select2();
		$("#perfil").select2();
		$("#nombre").focus();
	</script>